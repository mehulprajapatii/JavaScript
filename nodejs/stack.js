//*****************************************************************************
//Server.js
//*****************************************************************************
//

/*
 * Interactive Stack in JavaScript.
 * Rajesh Patkar Institute Of Software Engineering.
 * copyright ©  Rajesh Patkar, All rights reserved.
 */
 
 
var rpise = {};
rpise.rl = require('readline');
rpise.console = rpise.rl.createInterface(process.stdin, process.stdout,
        function(sub) {
            return [['push', 'pop', 'print', 'quit'], sub];
        }
);
rpise.stack = {
    stk: new Array(10),
    sp: 10,
    push: function(v) {
        if (this.sp == 0) {
            console.log("Overflow");
            throw "Overflow";
        }
        else {
            this.sp = this.sp - 1;
            this.stk[this.sp] = v;
        }
    },
    pop: function() {
        if (this.sp == 10) {
            console.log("Underflow");
            throw "Underflow";
        }
        else {
            this.sp = this.sp + 1;
            return this.stk[this.sp - 1];
        }
    },
    print: function() {
        console.log("Printing Stack");
        for (var i = this.sp; i < 10; i++)
        {
            console.log(this.stk[i]);
        }
    }
};

rpise.controller = function(input)
{
    switch (input)
    {
        case 'push' :
            rpise.console.removeAllListeners("line");
            rpise.console.on("line",
                    function pushController(value) {
                        try {
                            rpise.stack.push(value);
                            console.log("Thankyou for the input");
                        } catch (e) {
                            console.log("Sorry stack is full" +
                                    ".. pop to create space in stack");
                        } finally {
                            rpise.mainPrompt();
                        }
                    }
            );
            rpise.commandPrompt('push>');
            console.log("Enter the value to be pushed");
            break;
        case 'pop'  :
            try {
                console.log("the Last value popped is " + rpise.stack.pop())
            }
            catch (e) {
                console.log("Stack Empty value cannot be popped");
            }
            finally {
                rpise.mainPrompt();
            }
            break;
        case 'print':
            rpise.stack.print();
            rpise.mainPrompt();
            break;
        case 'quit' :
            rpise.console.close();
            process.stdin.destroy();
            break;
        default :
            console.log("Please enter proper command [tab for help]\n");
            break;
    }
}
rpise.mainPrompt = function()
{
    rpise.console.removeAllListeners("line");
    rpise.console.on("line", rpise.controller);
    rpise.console.setPrompt("rpise stack>", 12);
    rpise.console.prompt();
}
rpise.commandPrompt = function commandPrompt(command) {
    rpise.console.setPrompt(command, 6);
    rpise.console.prompt();
}

rpise.boot = function() {
    console.log("\nWelcome to RPISE Stack [press tab for help]\n");
    rpise.console.close = function() {
        console.log("\nThank your for using RPISE Stack ... \n");
    }
    rpise.mainPrompt();
}
rpise.boot();